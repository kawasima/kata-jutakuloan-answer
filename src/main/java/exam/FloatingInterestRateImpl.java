package exam;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FloatingInterestRateImpl implements FloatingInterestRate {
    private final LinkedList<MonthAndRate> rates = new LinkedList<>();

    public FloatingInterestRateImpl(BigDecimal rate) {
        rates.add(new MonthAndRate(RepaymentMonth.LOWER, rate));
    }

    @Override
    public BigDecimal getRate(RepaymentMonth month) {
        MonthAndRate monthAndRate = rates.stream()
                .filter(mar -> mar.month.isBefore(month))
                .findFirst()
                .orElseThrow();
        return monthAndRate.rate;
    }

    @Override
    public BigDecimal getRate(RepaymentMonth month, RepaymentMonth startMonth) {
        return getRate(month);
    }

    @Override
    public void addRate(RepaymentMonth month, BigDecimal rate) {
        int index = IntStream.range(0, rates.size())
                .filter(i -> rates.get(i).month.isBefore(month))
                .findFirst()
                .orElseThrow();
        rates.add(index, new MonthAndRate(month, rate));
    }

    private static class MonthAndRate {
        private final BigDecimal rate;
        private final RepaymentMonth month;

        public MonthAndRate(RepaymentMonth month, BigDecimal rate) {
            this.rate = rate;
            this.month = month;
        }
    }

    @Override
    public String toString() {
        return rates.stream()
                .map(mr -> mr.month + " " + mr.rate)
                .collect(Collectors.joining("\n"));
    }
}
