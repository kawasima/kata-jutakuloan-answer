package exam;

import java.math.BigDecimal;

public class InterestRateFactoryImpl implements InterestRateFactory {
    @Override
    public FixedInterestRate createFixedInterestRate(BigDecimal rate) {
        return new FixedInterestRateImpl(rate);
    }

    @Override
    public FixedInterestRate createLimitedFixedInterestRate(BigDecimal rate, int term, FloatingInterestRate interestRateAfterFixed) {
        return new FixedInterestRateImpl(rate, term, interestRateAfterFixed);
    }

    @Override
    public FloatingInterestRate createFloatingInterestRate(BigDecimal rate) {
        return new FloatingInterestRateImpl(rate);
    }
}
