package exam;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PrincipalAndInterestEqualRepaymentMethod implements RepaymentMethod {
    private final BigDecimal amountOfMoneyBorrowed;
    private final int numberOfRepayments;

    public PrincipalAndInterestEqualRepaymentMethod(BigDecimal amountOfMoneyBorrowed, int numberOfRepayments) {
        this.amountOfMoneyBorrowed = amountOfMoneyBorrowed;
        this.numberOfRepayments = numberOfRepayments;
    }


    private BigDecimal amountOfRepayment(BigDecimal interestRatePerMonth) {
        return amountOfMoneyBorrowed.multiply(interestRatePerMonth).multiply(
                interestRatePerMonth.add(BigDecimal.ONE).pow(numberOfRepayments)
        ).divide(interestRatePerMonth.add(BigDecimal.ONE).pow(numberOfRepayments).subtract(BigDecimal.ONE), RoundingMode.UP);
    }

    @Override
    public BigDecimal calcAmountOfPrincipalRepayment(BigDecimal interestRatePerMonth, BigDecimal balance) {
        return amountOfRepayment(interestRatePerMonth).subtract(calcAmountOfInterestRepayment(interestRatePerMonth, balance))
                .setScale(0, RoundingMode.UP);
    }

    @Override
    public BigDecimal calcAmountOfInterestRepayment(BigDecimal interestRatePerMonth, BigDecimal balance) {
        return balance.multiply(interestRatePerMonth).setScale(0, RoundingMode.UP);
    }
}
