package exam;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Stream;

public class LoanImpl implements Loan {
    private final BigDecimal amountOfMoneyBorrowed;
    private final InterestRate interestRate;
    private final int repaymentTerm;
    private final RepaymentMethod repaymentMethod;

    public LoanImpl(LoanCondition condition) {
        this.amountOfMoneyBorrowed = condition.getAmountOfMoneyBorrowed();
        this.interestRate = condition.getInterestRate();
        this.repaymentTerm = condition.getRepaymentTerm();
        switch(condition.getRepaymentMethod()) {
            case PRINCIPAL_AND_INTEREST_EQUAL:
                this.repaymentMethod = new PrincipalAndInterestEqualRepaymentMethod(amountOfMoneyBorrowed, numberOfRepayments());
                break;
            case PRINCIPAL_EQUAL_MONTHLY:
                this.repaymentMethod = new PrincipalEqualMonthlyRepaymentMethod(amountOfMoneyBorrowed, numberOfRepayments());
                break;
            default:
                throw new IllegalArgumentException("Unknown Repayment Method");
        }
    }

    private int numberOfRepayments() {
        return repaymentTerm * 12;
    }

    @Override
    public Stream<RepaymentPerMonth> repaymentStream(RepaymentMonth startMonth) {
        BigDecimal initialPrincipal = repaymentMethod.calcAmountOfPrincipalRepayment(interestRate.getRate(startMonth, startMonth).divide(new BigDecimal("12"), RoundingMode.UP), amountOfMoneyBorrowed);
        BigDecimal initialInterest = repaymentMethod.calcAmountOfInterestRepayment(interestRate.getRate(startMonth, startMonth).divide(new BigDecimal("12"), RoundingMode.UP), amountOfMoneyBorrowed);
        BigDecimal initialBalance = amountOfMoneyBorrowed.subtract(initialPrincipal).setScale(0, RoundingMode.UP);
        return Stream.iterate(new RepaymentPerMonth(1, startMonth, initialPrincipal, initialInterest, initialBalance),
                rpm -> {
                    RepaymentMonth month = rpm.getMonth().nextMonth();
                    BigDecimal principalRepayment = repaymentMethod.calcAmountOfPrincipalRepayment(interestRate.getRate(startMonth, month).divide(new BigDecimal("12"), RoundingMode.UP), rpm.getBalance());
                    BigDecimal interestRepayment = repaymentMethod.calcAmountOfInterestRepayment(interestRate.getRate(startMonth, month).divide(new BigDecimal("12"), RoundingMode.UP), rpm.getBalance());
                    int times = rpm.getTimes() + 1;
                    if (times == numberOfRepayments()) {
                        principalRepayment = rpm.getBalance();
                    }
                    return new RepaymentPerMonth(rpm.getTimes() + 1, month, principalRepayment, interestRepayment, rpm.getBalance().subtract(principalRepayment));
                }).limit(numberOfRepayments());
    }
}
