package exam;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PrincipalEqualMonthlyRepaymentMethod implements RepaymentMethod{
    private final BigDecimal amountOfMoneyBorrowed;
    private final int numberOfRepayments;

    public PrincipalEqualMonthlyRepaymentMethod(BigDecimal amountOfMoneyBorrowed, int numberOfRepayments) {
        this.amountOfMoneyBorrowed = amountOfMoneyBorrowed;
        this.numberOfRepayments = numberOfRepayments;
    }

    @Override
    public BigDecimal calcAmountOfPrincipalRepayment(BigDecimal interestRatePerMonth, BigDecimal balance) {
        return amountOfMoneyBorrowed.divide(new BigDecimal(numberOfRepayments), RoundingMode.UP).setScale(0, RoundingMode.UP);
    }

    @Override
    public BigDecimal calcAmountOfInterestRepayment(BigDecimal interestRatePerMonth, BigDecimal balance) {
        return balance.multiply(interestRatePerMonth).setScale(0, RoundingMode.UP);
    }
}
