package exam;

public class LoanFactoryImpl implements LoanFactory {
    @Override
    public Loan create(LoanCondition condition) {
        return new LoanImpl(condition);
    }
}
