package exam;

import java.math.BigDecimal;

public class FixedInterestRateImpl implements FixedInterestRate {
    private final BigDecimal rate;
    private Integer term;
    private FloatingInterestRate interestRateAfterFixed;

    public FixedInterestRateImpl(BigDecimal rate) {
        this.rate = rate;
    }

    public FixedInterestRateImpl(BigDecimal rate, int term, FloatingInterestRate interestRateAfterFixed) {
        this(rate);
        this.term = term;
        this.interestRateAfterFixed = interestRateAfterFixed;
    }

    @Override
    public BigDecimal getRate(RepaymentMonth month) {
        if (term != null) throw new UnsupportedOperationException("Limited fixed interest must set the start month");

        return rate;
    }

    @Override
    public BigDecimal getRate(RepaymentMonth month, RepaymentMonth startMonth) {
        if (term == null) return rate;

        if (month.isBefore(startMonth.plusMonths(term * 12L))) {
            return rate;
        } else {
            return interestRateAfterFixed.getRate(month, startMonth);
        }
    }
}
